# -*- mode: sh -*-

# Automatically load compiled files
function source () {
    local file=$1
    if [[ ! ${file}.zwc -nt ${file} ]]; then
        if [[ -r ${file} && -w ${file:h} ]]; then zcompile "${file:P}"; fi
    fi
    builtin source "$@"
}

function load_etc () {
    local file="$ZDOTDIR/etc/$1"
    source "${file}"
}

# Plugin manager
source "$ZDOTDIR/lib/zplugin/zplugin.zsh"
autoload -Uz _zplugin
(( ${+_comps} )) && _comps[zplugin]=_zplugin

# load_etc "env.zsh"
load_etc "tmux.zsh"
load_etc "init.zsh"
load_etc "fuzzy.zsh"
