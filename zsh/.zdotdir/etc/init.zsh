# Misc
zplugin light hlissner/zsh-autopair
zplugin light zdharma/fast-syntax-highlighting
zplugin light zsh-users/zsh-autosuggestions

# Aliases
alias en='emacsclient -n'
alias ec='emacsclient -c'
alias e='emacsclient -cn'
alias sudo='sudo '
alias rm='trash-put'

local LS_CMD
local LS_CMD_FULL
if (( $+commands[exa] )); then
    LS_CMD='command exa --color=auto'
    LS_CMD_FULL="$LS_CMD -hlF --color-scale --group-directories-first"
    alias l="$LS_CMD_FULL"
    alias ls="$LS_CMD"
    alias dir="$LS_CMD"
    alias ll="$LS_CMD_FULL"
    alias la="$LS_CMD_FULL -a --git"
    alias lad="$LS_CMD .*(.)"
    alias lsl="$LS_CMD_FULL *(@)"
    alias lsx="$LS_CMD_FULL *(*)"
    alias lt="$LS_CMD --tree"
fi

alias zrc='e $ZDOTDIR'
alias b=bat
alias p='popd > /dev/null'

# Completion
compdef _gnu_generic emacs emacsclient
compdef _fd fd
zplugin light zsh-users/zsh-completions

function _cm_expand_file_name () {
    local filename=$1
    echo "${filename/#\~/$HOME}"
}

# GUI

function _cm_copy_from_file () {
    xclip -selection clipboard -t $(file --mime-type -b $1) -i $1
}

function _cm_copy_file_path () {
    _cm_expand_file_name $1 | xclip -selection clipboard -i
}

alias clcp=_cm_copy_from_file
alias clpa=_cm_copy_file_path
