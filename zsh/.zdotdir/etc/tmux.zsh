if (( $+commands[tmux] )); then
    compdef _tmux _zsh_tmux_plugin_run

    function cm_run_tmux () {
        local -a tmux_cmd
        tmux_cmd=(command tmux)

        if [[ -n "$@" ]]; then
            $tmux_cmd "$@"
            return $?
        fi

        $tmux_cmd attach || $tmux_cmd new-session

    }

    alias tmux='cm_run_tmux'
    # Run tmux when start a terminal
    if [[ -z "$TMUX" && -z "$INSIDE_EMACS" && -z "$EMACS" && -z "$VIM" ]]; then
        cm_run_tmux
    fi
fi
