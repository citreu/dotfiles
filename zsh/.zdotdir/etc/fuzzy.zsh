local FUZZY_FINDER=sk
local FUZZY_FINDER_PLUGIN

if (( $+commands[${FUZZY_FINDER}] )); then
    if [[ ${FUZZY_FINDER} = fzf ]]; then
        FUZZY_FINDER_PLUGIN="$ZDOTDIR/lib/fzf-zsh"
        export FZF_CTRL_T_COMMAND="command fd -HL --color never 2>/dev/null"
        export FZF_ALT_C_COMMAND="command fd -HL --color never -t d 2>/dev/null"

    elif [[ ${FUZZY_FINDER} = sk ]]; then
        FUZZY_FINDER_PLUGIN="$ZDOTDIR/lib/skim-zsh"
        export SKIM_CTRL_T_COMMAND="command fd -HL --color never 2>/dev/null"
        export SKIM_ALT_C_COMMAND="command fd -HL --color never -t d 2>/dev/null"
    fi
    source $FUZZY_FINDER_PLUGIN/completion.zsh
    source $FUZZY_FINDER_PLUGIN/key-bindings.zsh
fi

function _cm_recent_visit_dir_internal () {
    local expanded
    local finder=$1
    local result=$(for d in $(dirs -v)
                   do
                       expanded=$(_cm_expand_file_name ${d})
                       if [[ -d ${expanded} ]]; then
                           echo ${expanded}
                       fi
                   done)
    cd -- $(echo ${result} | sort -u | command ${finder})
}

alias j="_cm_recent_visit_dir_internal ${FUZZY_FINDER}"
