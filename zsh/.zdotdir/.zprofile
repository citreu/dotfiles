# Environment
export LANG="en_US.UTF-8"
export DEFAULT_USER=$USER
export TERM=screen-256color
export EDITOR=emacsclient
export HISTSIZE=200000
export PATH="$(ruby -e 'print Gem.user_dir')/bin:\
$HOME/.cargo/bin:$PATH:\
$HOME/.local/bin"
export DIRSTACKSIZE=100000
