#!/usr/bin/env python2
from subprocess import check_output
import re
from os.path import expanduser

def get_pass(account):
    return check_output("pass mail/" + account, shell=True).splitlines()[0]

def get_pass_from_authinfo(server, login, port):
    s = "machine %s login %s port %s password ([^ ]*)\n" % (server, login, port)
    p = re.compile(s)
    with open(expanduser('~/.authinfo')) as f:
        result = f.read()
        return p.search(result).group(1)
