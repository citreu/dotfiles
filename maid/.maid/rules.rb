Maid.rules do

  Download_dir = "~/Downloads"

  def downloaded_files
    dir_safe(File.join(Download_dir, "*"))
  end

  def realext(file)
    File.extname(file).strip.downcase[1..-1]
  end

  def select_exts(files, exts)
    [].concat(*exts.map { |it| files.select { |f| realext(f) == it } })
  end

  def subdir(dirname)
    File.join(Download_dir, dirname)
  end

  rule 'MP3s' do
    puts select_exts(downloaded_files, ["epub"])
    select_exts(downloaded_files, ["mp3"]).each do |path|
      move(path, subdir("music"))
    end
  end

  rule 'Knowleges' do
    books_ext = ["pdf", "epub"]
    select_exts(downloaded_files, books_ext).each do |path|
      move(path, subdir("Books"))
    end
  end

  rule 'Videos' do
    video_files = ["mp4", "mkv"]
    select_exts(downloaded_files, video_files).each do |path|
      move(path, subdir("videos"))
    end
  end

  rule 'PKGBUILD' do
    downloaded_files
      .select { |it| File.exist?(File.join(it, "PKGBUILD")) }
      .each do |path|
      move(path, subdir("pkgbuild"))
    end
  end



end
